﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Auth;
using Firebase.Database;

public class FirebaseService : MonoBehaviour
{
    public static FirebaseService Instance {get; set;}

    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference rootRef;
    private DatabaseReference userRef;

    public bool cantRegister;
    public bool cantLogin;
    public bool[] Achivements;
    public int HighScore;

    public string detailHandle;


    private void Awake() {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(transform.gameObject);
            Instance = this;
        }
    }

    private void Start() {
        auth = FirebaseAuth.DefaultInstance;
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://chimica-project.firebaseio.com/");
        rootRef = FirebaseDatabase.DefaultInstance.RootReference;
        userRef = rootRef.Child("Users");
        cantLogin = cantRegister = false;
        user = auth.CurrentUser;
        Achivements = null;
        if(user != null)
        {
            SceneLoaderScript.Instance.LoadMainScene();
            retriveArchivement();
        }

    }

    public async void register(string email, string password, string displayName){
        await auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                cantRegister = true;
                return;
            }

            // Firebase user has been created.
            user = auth.CurrentUser;
        });
        UserProfile profile = new UserProfile();
        profile.DisplayName = displayName;
        if (auth.CurrentUser != null)
        {
            await user.UpdateUserProfileAsync(profile).ContinueWith(res => {
                if (res.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (res.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + res.Exception);
                    return;
                }

                Debug.Log("User profile updated successfully.");
                Debug.LogFormat("Firebase user created successfully: DisplayName:{0}, UID:({1})", user.DisplayName, user.UserId);
            });
            await rootRef.Child(user.UserId).SetRawJsonValueAsync(JsonUtility.ToJson(new User(user.UserId)));
            SceneLoaderScript.Instance.LoadMainScene();
            cantRegister = false;
        }
    }

    public async void login(string email, string password){
        await auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                cantLogin =true;
                return;
            }

            user = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                user.DisplayName, user.UserId);
            cantLogin = false;
        });
        if(user != null)
        {
            SceneLoaderScript.Instance.LoadMainScene();
        }
    }

    public void logout()
    {
        auth.SignOut();
        user = null;
    }

    public async void retriveArchivement(){
        //Debug.Log(user.UserId);
        await FirebaseDatabase.DefaultInstance.GetReference(user.UserId)
        .GetValueAsync().ContinueWith(task =>{
            if(task.IsFaulted || task.IsCanceled){
                Debug.Log("it fail");
            }
            else if(task.IsCompleted){
                DataSnapshot snapshot = task.Result;
                var HighScore = snapshot.Child("HighScore").Value;
                Achivements = new bool[snapshot.Child("Achivements").ChildrenCount];
                int index = 0;
                foreach(DataSnapshot data in snapshot.Child("Achivements").Children){
                    Achivements[index] = (bool)data.Value;
                    index++;
                }

                //Debug.Log(HighScore + "  " + Achivements);
            }
        });
    }
    public string getDisplayName()
    {
        return user.DisplayName;
    }
    
    public async void getCardDetail(Card card)
    {
        

        //Debug.Log(rootRef.Child("Card").Child("list").Child(card.atomType.ToString()).Child(card.fullName));
        await rootRef.Child("Card").Child("list").Child(card.atomType.ToString()).Child(card.fullName).Child("Detail").GetValueAsync().ContinueWith(task =>
        {
            DataSnapshot snapshot = task.Result;
            detailHandle = (string)snapshot.Value;
            
        });

        Debug.Log(detailHandle);
    }

    public void setHighSCore(float score)
    {
        if(score > HighScore)
        {
            userRef.Child(user.UserId).Child("HighScore").SetValueAsync(score);
        }
    }
}