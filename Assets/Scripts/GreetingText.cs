﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using TMPro;

public class GreetingText : MonoBehaviour
{
    public TextMeshProUGUI greetingText;

    private FirebaseAuth auth;
    private Firebase.Auth.FirebaseUser user;

    private void Awake()
    {
        auth = FirebaseAuth.DefaultInstance;
        user = auth.CurrentUser;
    }
    private void Start()
    {
        if(user != null)
            greetingText.text = "Hello, " + user.DisplayName;
        else
            greetingText.text = "No user loged-in";
    }
    private void Update()
    {
        
    }
}
