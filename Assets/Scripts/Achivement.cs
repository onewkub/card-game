﻿using UnityEngine;
using TMPro;
public class Achivement : MonoBehaviour
{
    public string ach_name;
    private TextMeshProUGUI ach_text;
    public bool state;

    private void Start() {
        state = false;
        ach_text = transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
    }
    private void Update() {
        if(state){
            ach_text.text = ach_name;
        }
    }
}
