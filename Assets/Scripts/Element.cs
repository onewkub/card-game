﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Element : MonoBehaviour
{
    private TextMeshProUGUI Symbol;
    private Button thisButton;
    public Card card;
    private void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(TaskOnClick);
        Symbol = transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
    }
    private void Update()
    {
        if(card != null)
        {
            Symbol.text = card.symbol;
        }
    }
    void TaskOnClick()
    {
        SceneLoaderScript.Instance.HandleCard = card;
    }
}
