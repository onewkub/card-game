﻿public enum AtomTypeEnum
{
    Alkali_earth,
    Alkali_metal,
    Halogen,
    Nobel_gas,
    Non_metal
}
