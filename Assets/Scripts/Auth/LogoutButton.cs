﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
public class LogoutButton : MonoBehaviour
{
    public void LogOut()
    {
        Debug.Log("LogOut");
        FirebaseService.Instance.logout();
        SceneLoaderScript.Instance.LoadLoginScene();
    }
}
