﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class RegisterButton : MonoBehaviour
{
    public TMP_InputField email;
    public TMP_InputField displayName;
    public TMP_InputField password;
    public TMP_InputField re_password;
    
    public GameObject warning;
    public Button registerButton;
    private bool State;

    private void Start()
    {
        State = false;

    }

    private void Update()
    {
        if (password.text == re_password.text && IsEmail(email.text) && !string.IsNullOrEmpty(password.text) && password.text.Length >= 8) State = true;
        else State = false;
        registerButton.interactable = State;
        warning.SetActive(FirebaseService.Instance.cantRegister);
    }

    public void OnRegister()
    {
        FirebaseService.Instance.register(email.text, password.text, displayName.text);
    }


    private const string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
          + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
          + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
          + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public static bool IsEmail(string email)
    {
        if (email != null) return System.Text.RegularExpressions.Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }


}

