using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoginButton : MonoBehaviour
{
    public TMP_InputField email;
    public TMP_InputField password;
    public GameObject warning;

    private void Update() {
        warning.SetActive(FirebaseService.Instance.cantLogin);
    }
    public void OnLogin()
    {
        FirebaseService.Instance.login(email.text, password.text);
    }
}
