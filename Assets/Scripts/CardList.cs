﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardList : MonoBehaviour
{
    public List<Card> Akali;
    public List<Card> Akali_e;
    public List<Card> Halogen;
    public List<Card> NobleGas;
    public List<Card> Non_met;
    private void Start()
    {
        int caseSwitch = SceneLoaderScript.Instance.handlePage;
        //Debug.Log(caseSwitch + " " + SceneLoaderScript.Instance.getHandlePage());
        switch (caseSwitch)
        {
            case 0:
                DisplayElement(Akali);
                break;

            case 1:
                DisplayElement(Akali_e);
                break;

            case 2:
                DisplayElement(Halogen);
                break;

            case 3:
                DisplayElement(NobleGas);
                break;

            case 4:
                DisplayElement(Non_met);
                break;

            default:
                Debug.Log("It's error");
                break;
        }
    }
    private void DisplayElement(List<Card> list)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if(i < list.Count)
            {
                //Debug.Log(i + " " + list.Count);
                gameObject.transform.GetChild(i).gameObject.GetComponent<Element>().card = list[i];
            }
            else
            {
                gameObject.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}
