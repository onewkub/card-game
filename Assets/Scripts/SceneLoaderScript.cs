﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoaderScript : MonoBehaviour
{
    public static SceneLoaderScript Instance { get; set;}
    public int handlePage;
    public Card HandleCard;
    public Player[] playerList;

    private int CurrentSceneIndex;
    private Stack<int> sceneStack;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(transform.gameObject);
            Instance = this;
        }

    }

    private void Start()
    {
        setVerical(); 
        sceneStack = new Stack<int>();
        CurrentSceneIndex = SceneManager.GetActiveScene().buildIndex;


    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Back to last scene");
            BackToTarget();
        }
    }
    private void LoadScene(int index)
    {
        Instance.CurrentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        //Debug.Log("Current Scene: " + SceneManager.GetActiveScene().name);
        Instance.sceneStack.Push(Instance.CurrentSceneIndex);
        SceneManager.LoadScene(index);
        Instance.CurrentSceneIndex = index;
    }
    private void LoadPreviosScene(int target)
    {
        SceneManager.LoadScene(target);
    }

    public void clearStack()
    {
        Instance.sceneStack.Clear();
    }
    public void BackToTarget()
    {
        int target;
        if (Instance.sceneStack.Count > 0)
        {
            target = Instance.sceneStack.Pop();
            //Debug.Log("Target: "+target);
            LoadPreviosScene(target);

        }
        else
        {
            Debug.Log("Exit...");
            Application.Quit();

        }
    }
    public void LoadLoginScene()
    {
        //Debug.Log("Load login Scene");
        setVerical();
        clearStack();
        LoadScene(0);
    }
    public void LoadRegisterScene()
    {
        //Debug.Log("Load register Scene");
        setVerical();
        LoadScene(1);
    }
    public void LoadMainScene()
    {
        //Debug.Log("Load Main Scene");
        LoadScene(2);
    }

    public void LoadAchivementScene()
    {
        setVerical();
        //Debug.Log("Load Achivement Scene");
        LoadScene(3);
    }

    public void LoadLearningScene()
    {
        setVerical();
        //Debug.Log("Load Achivement Scene");
        LoadScene(4);
    }

    public void LoadTopicCardDetailScene()
    {
        setVerical();
        //Debug.Log("Load Achivement Scene");
        LoadScene(5);
    }

    public void LoadCardTemplateScene(int page)
    {
        setVerical();
        if (page >= 0)
        {
            Instance.handlePage = page;
        }
        //Debug.Log("Page: " + page.ToString() + " : " + handlePage);
        //Debug.Log("Load Achivement Scene");
        LoadScene(6);
    }

    public void LoadCardDetailTemplateScene()
    {
        setVerical();
        //Debug.Log("Load Achivement Scene");
        LoadScene(7);
    }

    public void LoadGameScene()
    {
        setHorizontal();
        LoadScene(9);
    }

    public void LoadResultScene()
    {
        setVerical();
        SceneManager.LoadScene(8);
    }
    private void setVerical()
    {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void setHorizontal()
    {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = ScreenOrientation.Landscape;
    }
}
