class User
{
    public string UID;
    public int HighScore;
    public bool[] Achivements;

    public User(string uid){
        UID = uid;
        HighScore = 0;
        Achivements = new bool[9];
    }
}