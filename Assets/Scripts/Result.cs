﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Result : MonoBehaviour
{
    public TextMeshProUGUI Status;
    public TextMeshProUGUI Score;

    private void Start()
    {
        if(SceneLoaderScript.Instance.playerList != null)
        {
            //Debug.Log(SceneLoaderScript.Instance.playerList[0].getPlayerScore() + " : " + SceneLoaderScript.Instance.playerList[0].getPlayerScore());
            if (SceneLoaderScript.Instance.playerList[0].getPlayerScore() > SceneLoaderScript.Instance.playerList[1].getPlayerScore()) Status.text = "Win";
            else if (SceneLoaderScript.Instance.playerList[0].getPlayerScore() < SceneLoaderScript.Instance.playerList[1].getPlayerScore()) Status.text = "Lose";
            else Status.text = "Draw";
            Score.text = "Score: " + (SceneLoaderScript.Instance.playerList[0].getPlayerScore().ToString("0"));
            FirebaseService.Instance.setHighSCore(SceneLoaderScript.Instance.playerList[0].getPlayerScore());
        }

    }
}
