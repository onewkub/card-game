﻿using UnityEngine;
using TMPro;
public class Achivements : MonoBehaviour
{
    [SerializeField] private Achivement[] AchivementList;
    private bool hasSet = false;
    public TextMeshProUGUI HighScoreText;
    private void Start() {
        AchivementList = new Achivement[transform.childCount];
        for(int i = 0; i < transform.childCount; i++){
            AchivementList[i] = transform.GetChild(i).gameObject.GetComponent<Achivement>();
        }
    }
    private void Update() {
        if(FirebaseService.Instance.Achivements != null && !hasSet){
            for(int i = 0; i < transform.childCount; i++){
                AchivementList[i].state = FirebaseService.Instance.Achivements[i];
            }
            HighScoreText.text = "Your high score: " + FirebaseService.Instance.HighScore;
            hasSet = true;
        }
 
    }

}
