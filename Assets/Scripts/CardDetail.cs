﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CardDetail : MonoBehaviour
{
    public Text Topic;
    public Text Detail;
    public Card card;
    private void Start()
    {
        card = SceneLoaderScript.Instance.HandleCard;
        Topic.text = card.name;
        Detail.text = "";
        FirebaseService.Instance.detailHandle = "";
        FirebaseService.Instance.getCardDetail(card);
    }

    private void Update()
    {
        //Debug.Log(FirebaseService.Instance.detailHandle);
        if (FirebaseService.Instance.detailHandle != null)
        {
            Detail.text = FirebaseService.Instance.detailHandle;    

        }
    }
}
