﻿using UnityEngine;
using TMPro;

public class Card3DDisplayV2 : MonoBehaviour
{
    public TextMeshProUGUI Symbol;
    public TextMeshProUGUI fullName;
    private string[] prefixNum = { "", "Mono", "Di", "Tri", "Tetra", "Penta", "Hexa", "Hepta", "Octa", "Nona", "Deca" };
    public void GenerateNameAndSymbol(Card positiveAtom, int positiveCharge, Card negativeAtom, int negativeCharge)
    {
        string symbol = "";
        if (positiveCharge == 1 && negativeCharge == 1)
            symbol = positiveAtom.symbol + negativeAtom.symbol;
        else if(positiveCharge == 1 && negativeCharge > 1)
            symbol = positiveAtom.symbol + negativeAtom.symbol + "<sub>" + (negativeCharge - 1).ToString() + "</sub>";
        else if(positiveCharge > 1 && negativeCharge == 1)
            symbol = positiveAtom.symbol + "<sub>" + (positiveCharge - 1).ToString() +"</sub>" + negativeAtom.symbol;
        else if (positiveCharge > 1 && negativeCharge > 1)
            symbol = positiveAtom.symbol + "<sub>" + (positiveCharge - 1).ToString() +
            "</sub>" + negativeAtom.symbol + "<sub>" + (negativeCharge - 1).ToString() + "</sub>";


        Symbol.text = symbol;
        fullName.text = "";

        if (positiveCharge == 1) fullName.text += positiveAtom.fullName;
        else fullName.text += prefixNum[positiveCharge] + positiveAtom.fullName;
        if (negativeCharge == 1) fullName.text += negativeAtom.negativeName;
        else fullName.text += prefixNum[negativeCharge] + negativeAtom.negativeName;


    }
    public void GenerateNoble(Card card)
    {
        Symbol.text = card.symbol;
        fullName.text = card.fullName;
    }
}
