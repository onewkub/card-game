﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{
    public GameManager gameManager;
    public void BotStart(GameObject PlayerHand)
    {
        if (GameManager.Instance.timer > 5) return;
        Debug.Log("bot working");
        for(int i = 0; i < PlayerHand.transform.childCount; i++)
        {
            var currentCard = PlayerHand.transform.GetChild(i).gameObject.GetComponent<CardDisplay>();
            if (currentCard.card.charge == 0) currentCard.SelectCard();
        }
        //gameManager.PlayerThrowCard();
        
        for (int i = 0; i < PlayerHand.transform.childCount; i++)
        {
            var currentCard = PlayerHand.transform.GetChild(i).gameObject.GetComponent<CardDisplay>();
            for (int j = 0; j < PlayerHand.transform.childCount; j++)
            {
                var subCurrentCard = PlayerHand.transform.GetChild(i).gameObject.GetComponent<CardDisplay>();

                if (i == j) continue;
                else if (currentCard.card.charge + subCurrentCard.card.charge == 0)
                {
                    currentCard.SelectCard();
                    subCurrentCard.SelectCard();
                }
            }
        }
        gameManager.PlayerThrowCard();

        //gameManager.PlayerPass();
    }
}
